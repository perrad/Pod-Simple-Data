# NAME

Pod::Simple::Data -- retrieve the data inlined in Pod

<div>

    <a href="http://cpants.charsbar.org/dist/overview/Pod-Simple-Data"><img alt="Kwalitee Status" src="http://cpants.cpanauthors.org/dist/Pod-Simple-Data.png" /></a>
    <a href="http://badge.fury.io/pl/Pod-Simple-Data"><img alt="CPAN version" src="http://badge.fury.io/pl/Pod-Simple-Data.svg" /></a>
</div>

# SYNOPSIS

    perl -MPod::Simple::Data -e \
     "exit Pod::Simple::Data->new('stuff', 'xstuff')->parse_file(shift)->any_errata_seen" \
     thingy.pod

# DESCRIPTION

This class is that for retrieving data from `=begin/=for/=end` sections.
The constructor accepts a list of identifier, the default is `'*'` which allows
to retrieve all data.

This is a subclass of [Pod::Simple](https://metacpan.org/pod/Pod::Simple) and inherits all its methods.

# CALLING FROM THE COMMAND LINE

    perl -MPod::Simple::Data -e \
     "exit Pod::Simple::Data->new()->parse_file(shift)->any_errata_seen" \
     thingy.pod

# CALLING FROM PERL

## Minimal code

    use Pod::Simple::Data;
    my $p = Pod::Simple::Data->new();
    $p->output_string(\my $data);
    $p->parse_file('path/to/Module/Name.pm');
    open my $out, '>', 'out.dat' or die "Cannot open 'out.dat': $!\n";
    print $out $data;

# SEE ALSO

[Pod::Simple](https://metacpan.org/pod/Pod::Simple), ["About Data Paragraphs" in perlpodspec](https://metacpan.org/pod/perlpodspec#About-Data-Paragraphs),

# AUTHOR

Francois Perrad <francois.perrad@gadz.org>

# COPYRIGHT

Copyright 2014 Francois Perrad

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.
